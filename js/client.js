$(function () {
    baguetteBox.run('.photo-gallery', {
        animation: 'fadeIn',
        noScrollbars: true
    });
});